﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.OpenApi.Models;

using Swashbuckle.AspNetCore.SwaggerGen;


namespace AddressBook.Helpers
{
    // https://www.seeleycoder.com/blog/optional-route-parameters-with-swagger-asp-net-core/
    public class ReApplyOptionalRouteParameterOperationFilter : IOperationFilter
    {
        private const String CaptureName = "routeParameter";


        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            var httpMethodAttributes = context.MethodInfo.GetCustomAttributes(true).OfType<HttpMethodAttribute>();
            // Check for Template != null added
            var httpMethodWithOptional = httpMethodAttributes?.FirstOrDefault(s => s.Template != null && s.Template.Contains("?"));

            if(httpMethodWithOptional == null) return;

            var regex = $"{{(?<{CaptureName}>\\w+)\\?}}";
            var matches = Regex.Matches(httpMethodWithOptional.Template, regex);

            foreach (Match match in matches)
            {
                var name = match.Groups[CaptureName].Value;
                var parameter = operation.Parameters.FirstOrDefault(s => s.In == ParameterLocation.Path && s.Name.Equals(name));

                if (parameter != null)
                {
                    parameter.AllowEmptyValue = true;
                    parameter.Description = "Must check \"Send empty value\" or Swagger passes a comma for empty value otherwise.";
                    parameter.Required = false;
                    parameter.Schema.Nullable = true;
                }
            }
        }
    }
}
