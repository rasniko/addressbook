using AddressBook.Data.Postgres;
using AddressBook.Data.Repository;
using AddressBook.Helpers;
using AddressBook.Hubs;
using AddressBook.Services;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;


namespace AddressBook
{
    public class Startup
    {
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }


        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<AdContext>(options => options.UseNpgsql(_configuration.GetConnectionString("DefaultConnection"), opt => opt.MigrationsAssembly("AddressBook.Data.Postgres")));

            services.AddSignalR();

            services.AddSingleton<IProblemDetailsService, ProblemDetailsService>();
            services.AddScoped<IContactsRepository, PostgresContactsRepository>();
            services.AddScoped<IContactsService, RtContactsService>();

            services.AddControllers()
                .AddJsonOptions(options => options.JsonSerializerOptions.Converters.Add(new CustomDateTimeConverter()));

            services.AddSwaggerGen(opt =>
            {
                opt.OperationFilter<ReApplyOptionalRouteParameterOperationFilter>();
                opt.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "Address Book Api",
                    Description = "A simple example of address book api."
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();
            app.UseSwaggerUI(opt =>
            {
                opt.SwaggerEndpoint("v1/swagger.json", "Address Book API");
            });

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHub<AdHub>("/rtContacts");
            });
        }
    }
}
