﻿using AddressBook.Data;

using Microsoft.AspNetCore.SignalR;

using System.Threading.Tasks;


namespace AddressBook.Hubs
{
    public class AdHub : Hub
    {
        public async Task ContactUpdate(Contact contact)
        {
            await Clients.All.SendAsync("ContactUpdated", contact);
        }
    }
}
