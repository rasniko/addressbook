﻿using AddressBook.Data;
using AddressBook.Services;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using System;
using System.Collections.Generic;
using System.Net.Mime;
using Microsoft.AspNetCore.WebUtilities;


namespace AddressBook.Controllers
{
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Route("[controller]/[action]")]
    public class ContactsController : ControllerBase
    {
        private readonly IContactsService _contactsService;
        private readonly IProblemDetailsService _problemDetailsService;

        public ContactsController(IContactsService contactsService, IProblemDetailsService problemDetailsService)
        {
            _contactsService = contactsService;
            _problemDetailsService = problemDetailsService;
        }


        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<Contact> Create(Contact contact)
        {
            var result = _contactsService.CreateContact(contact);

            if(result.Success)
                return CreatedAtAction(nameof(Create), new { id = contact.Id }, contact);

            return StatusCode(StatusCodes.Status400BadRequest, _problemDetailsService.GetDetails(StatusCodes.Status400BadRequest, result.Message));
        }



        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<Contact> Get(Int32 id)
        {
            var contact = _contactsService.GetContact(id);

            if (contact == null)
            {
                return StatusCode(StatusCodes.Status404NotFound, _problemDetailsService.GetDetails(StatusCodes.Status404NotFound));
            }

            return Ok(contact);
        }

        [HttpGet("{skip?}/{take?}")]
        public ActionResult<List<Contact>> GetMultiple(Int32? skip = null, Int32? take = null)
        {
            return Ok(_contactsService.GetContacts(null, skip, take));
        }

        [HttpGet("{name}/{skip?}/{take?}")]
        public ActionResult<List<Contact>> GetMultiple(String name, Int32? skip = null, Int32? take = null)
        {
            return Ok(_contactsService.GetContacts(name, skip, take));
        }



        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult Delete(Int32 id)
        {
            var result = _contactsService.DeleteContact(id);

            if (result.Success) 
                return Ok("Contact deleted successfully.");

            return StatusCode(StatusCodes.Status404NotFound, _problemDetailsService.GetDetails(StatusCodes.Status404NotFound, result.Message));
        }



        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<Contact> Update(Contact contact)
        {
            if (contact != null && contact.Id == 0)
                return StatusCode(StatusCodes.Status400BadRequest,_problemDetailsService.GetDetails(StatusCodes.Status400BadRequest, "Contact id must be specified."));

            var result = _contactsService.UpdateContact(contact);

            if (result.Success)
                return Ok("Contact updated successfully.");

            return StatusCode(StatusCodes.Status400BadRequest, _problemDetailsService.GetDetails(StatusCodes.Status400BadRequest, result.Message));
        }
    }
}
