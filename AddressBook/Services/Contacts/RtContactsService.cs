﻿using AddressBook.Data;
using AddressBook.Data.Repository;
using AddressBook.Hubs;

using Microsoft.AspNetCore.SignalR;

using System;


namespace AddressBook.Services
{
    public class RtContactsService : ContactsService
    {
        private readonly IHubContext<AdHub> _hub;

        public RtContactsService(IContactsRepository contactsRepository, IHubContext<AdHub> hub) : base(contactsRepository)
        {
            _hub = hub;
        }



        public override StateModel CreateContact(Contact contact)
        {
            var result = base.CreateContact(contact);

            if(result.Success)
                _hub.Clients.All.SendAsync("ContactCreated", contact);

            return result;
        }



        public override StateModel DeleteContact(Int32 id)
        {
            var result = base.DeleteContact(id);

            if (result.Success)
                _hub.Clients.All.SendAsync("ContactDeleted", id);

            return result;
        }



        public override StateModel UpdateContact(Contact contact)
        {
            var result = base.UpdateContact(contact);

            if(result.Success)
                _hub.Clients.All.SendAsync("ContactUpdated", contact);

            return result;
        }
    }
}
