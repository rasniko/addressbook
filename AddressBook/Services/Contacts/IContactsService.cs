﻿using AddressBook.Data;

using System;
using System.Collections.Generic;


namespace AddressBook.Services
{
    public interface IContactsService
    {
        Contact GetContact(Int32 id);

        List<Contact> GetContacts(String name = null, Int32? skip = null, Int32? take = null);


        StateModel CreateContact(Contact contact);


        StateModel UpdateContact(Contact contact);


        StateModel DeleteContact(Int32 id);
    }
}
