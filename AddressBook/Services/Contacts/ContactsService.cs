﻿using AddressBook.Data;
using AddressBook.Data.Repository;

using System;
using System.Collections.Generic;


namespace AddressBook.Services
{
    public class ContactsService : IContactsService
    {
        private readonly IContactsRepository _contactsRepository;



        public ContactsService(IContactsRepository contactsRepository)
        {
            _contactsRepository = contactsRepository;
        }



        public virtual StateModel CreateContact(Contact contact)
        {
            return _contactsRepository.AddContact(contact);
        }



        public virtual StateModel DeleteContact(Int32 id)
        {
            return _contactsRepository.DeleteContact(id);
        }



        public Contact GetContact(Int32 id)
        {
            return _contactsRepository.GetContact(id);
        }

        public List<Contact> GetContacts(String name = null, Int32? skip = null, Int32? take = null)
        {
            return _contactsRepository.GetContacts(name, skip, take);
        }



        public virtual StateModel UpdateContact(Contact contact)
        {
            return _contactsRepository.UpdateContact(contact);
        }
    }
}
