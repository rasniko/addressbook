﻿using System;

using Microsoft.AspNetCore.Mvc;


namespace AddressBook.Services
{
    public interface IProblemDetailsService
    {
        ProblemDetails GetDetails(Int32 statusCode, String message = "");
    }
}
