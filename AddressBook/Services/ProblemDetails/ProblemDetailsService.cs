﻿using System;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;


namespace AddressBook.Services
{
    public class ProblemDetailsService : IProblemDetailsService
    {
        public ProblemDetails GetDetails(Int32 statusCode, String message = "")
        {
            return new ProblemDetails
            {
                Status = statusCode,
                Title = ReasonPhrases.GetReasonPhrase(statusCode),
                Detail = message
            };
        }
    }
}
