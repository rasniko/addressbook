﻿using System;


namespace AddressBook.Data
{
    public class StateModel
    {
        public StateModel(String message, Boolean success, Object data = null)
        {
            Message = message;
            Success = success;
            Data = data;
        }


        public Boolean Success { get; set; }
        public String Message { get; set; }

        public Object Data { get; set; }
    }
}
