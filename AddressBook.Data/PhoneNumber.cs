﻿using AddressBook.Data.Core;

using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;


namespace AddressBook.Data
{
    public class PhoneNumber : CoreEntity
    {
        [Required]
        [StringLength(50)]
        public String Number { get; set; }

        [StringLength(50)]
        public String Type { get; set; }

        [JsonIgnore]
        public Int32 ContactId { get; set; }
    }
}
