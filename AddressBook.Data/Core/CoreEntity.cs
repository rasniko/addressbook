﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;


namespace AddressBook.Data.Core
{
    public class CoreEntity
    {
        [Key]
        public Int32 Id { get; set; }

        [JsonIgnore]
        public DateTime Created { get; set; }
        [JsonIgnore]
        public DateTime Modified { get; set; }
    }
}
