﻿using System;
using System.Collections.Generic;


namespace AddressBook.Data.Repository
{
    public interface IContactsRepository
    {
        Contact GetContact(Int32 id);
        Contact GetContact(String name, String address);

        List<Contact> GetContacts(String name = null, Int32? skip = null, Int32? take = null);

        StateModel AddContact(Contact contact);

        StateModel DeleteContact(Int32 id);

        StateModel UpdateContact(Contact contact);
    }
}
