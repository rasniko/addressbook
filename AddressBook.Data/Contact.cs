﻿using AddressBook.Data.Core;

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace AddressBook.Data
{
    public class Contact : CoreEntity
    {
        [Required]
        [StringLength(100)]
        public String Name { get; set; }

        [Required]
        [StringLength(200)]
        public String Address { get; set; }

        [DataType(DataType.Date)]
        public DateTime? DateOfBirth { get; set; }


        public List<PhoneNumber> PhoneNumbers { get; set; }


        public override string ToString()
        {
            var result = $"{Name}| {Address}| {DateOfBirth:dd.MM.yyyy}";
            
            if(PhoneNumbers != null)
                foreach (var phoneNumber in PhoneNumbers)
                    result += $"\n{phoneNumber.Type}: {phoneNumber.Number}";

            return result;
        }
    }
}
