﻿using AddressBook.Data;

using Microsoft.AspNetCore.SignalR.Client;

using System;
using System.Threading.Tasks;


namespace AddressBook.Client
{
    static class Program
    {
        static HubConnection _connection;
        static Int32 _delay = 250;


        static void Main(string[] args)
        {
            ConfigureSignalRConnection();
            ConfigureCallbacks();

            if (!ConnectToSignalR())
            {
                Console.WriteLine("Unable to connect to server. Exiting ...");
                return;
            }
            
            Console.WriteLine($"Connected ...");

            Console.WriteLine("Press 'Enter' to exit!");
            Console.ReadLine();

            _connection.StopAsync().Wait();
        }

        
        
        private static void ConfigureSignalRConnection()
        {
            _connection = new HubConnectionBuilder()
                .WithUrl("https://localhost:5001/rtContacts")
                .Build();

            _connection.Closed += async (error) =>
            {
                Console.WriteLine(error);

                await Task.Delay(1000);
                await _connection.StartAsync();
            };
        }

        private static void ConfigureCallbacks()
        {
            _connection.On<Contact>("ContactCreated", (contact) => Console.WriteLine($"New contact added: \n{contact}\n"));
            _connection.On<Contact>("ContactUpdated", (contact) => Console.WriteLine($"Contact updated: \n{contact}\n"));
            _connection.On<Int32>("ContactDeleted", (id) => Console.WriteLine($"Contact with id '{id}' has been deleted."));
        }

        private static Boolean ConnectToSignalR()
        {
            try
            {
                _connection.StartAsync().Wait();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine($"Trying to reconnect in {_delay} ms ...");

                if (_delay < 8000) _delay += _delay;
                else return false;

                return ConnectToSignalR();
            }

            return true;
        }
    }
}
