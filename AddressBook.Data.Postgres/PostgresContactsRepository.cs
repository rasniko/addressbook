﻿using AddressBook.Data.Repository;

using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Linq;


namespace AddressBook.Data.Postgres
{
    public class PostgresContactsRepository : IContactsRepository
    {
        private readonly AdContext _adContext;

        public PostgresContactsRepository(AdContext adContext)
        {
            _adContext = adContext;
        }



        public StateModel AddContact(Contact contact)
        {
            if (!IsUniqueNameAddress(contact.Name, contact.Address))
                return new StateModel("Cannot create new contact with existing name and address.", false);

            _adContext.Contacts.Add(contact);
            _adContext.SaveChanges();

            return new StateModel("Created.", true, contact);
        }



        public StateModel DeleteContact(Int32 id)
        {
            var contact = GetContact(id);

            if (contact != null)
            {
                _adContext.Remove(contact);
                _adContext.SaveChanges();

                return new StateModel("Success.", true, contact);
            }

            return new StateModel($"Contact with id '{id}' doesn't exist.", false);
        }



        public Contact GetContact(Int32 id)
        {
            return _adContext.Contacts.Where(s => s.Id == id).Include(s => s.PhoneNumbers).SingleOrDefault();
        }

        public Contact GetContact(String name, String address)
        {
            return _adContext.Contacts
                .Where(s => s.Name.Equals(name) && s.Address.Equals(address))
                .Include(s => s.PhoneNumbers)
                .SingleOrDefault();
        }

        public List<Contact> GetContacts(String name = null, Int32? skip = null, Int32? take = null)
        {
            var query = GetContacts(name);

            if (skip.HasValue && take.HasValue)
                return query.Skip(skip.Value).Take(take.Value).Include(s => s.PhoneNumbers).ToList();

            return query.Include(s => s.PhoneNumbers).ToList();
        }

        private IQueryable<Contact> GetContacts(String name)
        {
            return !String.IsNullOrWhiteSpace(name)
                ? _adContext.Contacts.Where(s => EF.Functions.ILike(s.Name, $"%{name}%"))
                : _adContext.Contacts.AsQueryable<Contact>();
        }



        public StateModel UpdateContact(Contact contact)
        {
            var result = _adContext.Contacts.Where(s => s.Id == contact.Id).AsNoTracking().SingleOrDefault();
            if (result == null) return new StateModel($"Contact with id '{contact.Id}' doesn't exist.", false);

            if (!IsUniqueNameAddress(contact.Name, contact.Address, contact.Id))
                return new StateModel("Contact with same name and address already exists.", false);

            foreach(var phoneNumber in contact.PhoneNumbers)
            {
                UpdatePhoneNumberId(phoneNumber, contact.Id);
            }

            _adContext.Contacts.Update(contact);
            _adContext.SaveChanges();

            return new StateModel("Success.", true);
        }

        private void UpdatePhoneNumberId(PhoneNumber phoneNumber, Int32 id)
        {
            var result = _adContext.PhoneNumbers
                .Where(s => s.Number.Equals(phoneNumber.Number) && s.Type.Equals(phoneNumber.Type) && s.ContactId == id)
                .AsNoTracking()
                .SingleOrDefault();

            if (result != null)
                phoneNumber.Id = result.Id;
        }



        private Boolean IsUniqueNameAddress(String name, String address, Int32? id = null)
        {
            var result = _adContext.Contacts.Where(s => s.Name.Equals(name) && s.Address.Equals(address)).AsNoTracking().SingleOrDefault();

            if (result != null)
                return id.HasValue && id.Value == result.Id;

            return true;
        }
    }
}
