﻿using AddressBook.Data.Core;

using Microsoft.EntityFrameworkCore;

using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;


namespace AddressBook.Data.Postgres
{
    public class AdContext : DbContext
    {
        public AdContext(DbContextOptions<AdContext> options) : base(options)
        {
        }


        public DbSet<Contact> Contacts { get; set; }
        public DbSet<PhoneNumber> PhoneNumbers { get; set; }



        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Contact>().HasIndex(s => new { s.Name, s.Address }).IsUnique(true);
            modelBuilder.Entity<PhoneNumber>().HasIndex(s => new { s.Id, s.Number, s.Type }).IsUnique(true);
        }



        public override Int32 SaveChanges()
        {
            AddAuitInfo();

            return base.SaveChanges();
        }

        public override Task<Int32> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            AddAuitInfo();

            return base.SaveChangesAsync(cancellationToken);
        }



        private void AddAuitInfo()
        {
            foreach (var entry in ChangeTracker.Entries().Where(x => x.Entity is CoreEntity && (x.State == EntityState.Added || x.State == EntityState.Modified)))
            {
                if (entry.State == EntityState.Added)
                {
                    ((CoreEntity)entry.Entity).Created = DateTime.UtcNow;
                    ((CoreEntity)entry.Entity).Id = 0;
                }

                if( entry.State == EntityState.Modified)
                {
                    entry.Property("Created").IsModified = false;
                }

                ((CoreEntity)entry.Entity).Modified = DateTime.UtcNow;
            }
        }
    }
}
