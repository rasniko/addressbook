﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AddressBook.Migrations
{
    public partial class indexupd : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_PhoneNumbers_Id_Number_Type",
                table: "PhoneNumbers",
                columns: new[] { "Id", "Number", "Type" },
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_PhoneNumbers_Id_Number_Type",
                table: "PhoneNumbers");
        }
    }
}
