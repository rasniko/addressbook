# AddressBook

### Migrations

Run migrations on AddressBook.Data.Postgres project.

`dotnet ef database update --startup-project ../AddressBook`  

`-- startup-project ../AddressBook` is needed for all commands.